//用一个模块写出加减乘除的运算结果
module.exports = class comput{
    constructor(a,b){
        this.a=a;
        this.b=b;
    }
    addition(){
        return this.a + this.b;
    }
    subtraction(){
        return this.a - this.b;
    }
    multiplication(){
        return this.a * this.b;
    }
    division(){
        return this.a / this.b;
    }
}